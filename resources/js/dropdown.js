AddStyle('./../../resources/css/dropdown.css');

let Dropdowns = document.getElementsByClassName('dropdown');

for (let i of Dropdowns) {
    i.onmouseenter = () => {
        let elems = i.getElementsByClassName('dropup');
        elems[0].style.display = 'flex';
    }

    i.onmouseleave = () => {
        let elems = i.getElementsByClassName('dropup');
        elems[0].style.display = 'none';
    }
}