"use strict"


function CreateElement(type, id, _class, parent) {
    let n_elem = document.createElement(type);

    if (_class) n_elem.className = _class;
    if (id) n_elem.id = id;
    if (parent) parent.appendChild(n_elem);
    return n_elem;
}

function AddStyle(href) {
    let link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = href;
    document.head.appendChild(link);
}


function LoadModule(module, path) {
    if (document.modules && document.modules.includes(module))
        return;

    let tools = document.createElement('script');
    tools.src = path;
    document.head.appendChild(tools);
    document.modules.push(module);
}

function Input_OnEnter(input, callback) {
    input.onkeyup = function (e) {
        if (e.keyCode == 13) {
            callback();
            input.blur();
            input.value = "";
        }
    };
}

Math.clamp = function (number, down, up) {
    if (number > up) return up;
    else if (number < down) return down;
    else return number;
}

function Color(r, g, b, a) {
    let self = this;
    this.r = r || 0;
    this.g = g || 0;
    this.b = b || 0;
    this.a = a || 0;

    this.ToString = () => { return 'rgba(' + self.r + ',' + self.g + ',' + self.b + ',' + self.a + ')' };
}

Color.Random = function () {
    return new Color(Math.rnd_int(0, 255), Math.rnd_int(0, 255), Math.rnd_int(0, 255), Math.rnd_float(0.8, 1));
}

function Switch() {
    this.state = false;

    const self = function () {
        this.state = !this.state
        return !this.state;
    }

    return self;
}

function Fade(object, speed, callback, from) {
    object.alpha = from;
    let interval = setInterval(function () {
        object.alpha = Math.clamp(object.alpha + speed, 0, 1);
        if (object.alpha == 0 || object.alpha == 1) {
            clearInterval(interval);
            if (callback)
                callback();
        }
    }, 17);
}

function FadeCss(object, speed, callback, from) {
    let current = from;
    object.style.opacity = from;
    let interval = setInterval(() => {
        object.style.opacity = current = Math.clamp(current + speed, 0, 1);
        if (current == 0 || current == 1) {
            clearInterval(interval);
            if (callback) callback();
        }
    }, 17);
}

function BlurCss(object, speed, callback, from) {
    let current = from;
    object.style.filter = 'blur(' + current + 'px)';
    let interval = setInterval(() => {
        current = Math.clamp(current + speed, 0, 100);
        object.style.filter = 'blur(' + current + 'px)';
        if (current == 0 || current == 10) {
            clearInterval(interval);
            if (callback) callback();
        }
    }, 17);
}

function GetContentWidth(elem) {
    let sum = 0;
    for (let i = 0; i < elem.children.length; i++)
        sum += elem.children.item(i).clientWidth;

    return sum + 1;
}

function SlideableToggleHorizontal(elemId) {
    let elem = document.getElementById(elemId);
    if (elem.dt_open == 'undefined') elem.dt_open = false;
    elem.style.width = elem.dt_open ? '0' : GetContentWidth(elem) + 'px';
    elem.dt_open = !elem.dt_open

}

function SlideableToggleSize(elemId, sz) {
    let elem = document.getElementById(elemId);
    if (elem.dt_open == 'undefined') elem.dt_open = false;
    elem.style.width = elem.dt_open ? '0' : sz;
    elem.dt_open = !elem.dt_open
}

let SldTSz = SlideableToggleSize
let SldTH = SlideableToggleHorizontal;

window.onload = function () {
    let elem = document.getElementsByClassName('no_space');
    for (let i = 0; i < elem.length; i++) {
        let rect = document.getElementById(elem[i].getAttribute('dtools-pos-at')).getBoundingClientRect();
        elem[i].style.left = rect.left + 'px';
        elem[i].style.top = rect.top + 'px';
    }
};

Math.distance = Math.dist = function (a, b) {
    let dx = a.x - b.x;
    let dy = a.y - b.y;

    return Math.sqrt(dx * dx + dy * dy);
}

Math.rnd_int = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

Math.rnd_float = function (min, max) {
    return Math.random() * (max - min) + min;
}

Math.jump = function (number, lower, upper) {
    if (number > upper) return lower;
    else if (number < lower) return upper;
    else return number;
}

Math.snap = function (from, to) {
    return Math.round(from / to) * to;
}

if (!document.modules) document.modules = [];
document.modules.push('tools');

function Array2D(x, y) {
    return new Array(y).fill(null).map(() => new Array(x).fill(null));
}

Math.sqr = function (a) {
    return a * a;
}

Math.dist2 = function (a, b) {
    return Math.sqr(a.x - b.x) + Math.sqr(a.y - b.y);
}

function ClearChildren(element) {
    while (element.firstChild) element.removeChild(element.firstChild);
}