AddStyle('./../../resources/css/message.css');

function CreateMessage() {
    let self = this;
    let Container = CreateElement('div', '', 'layer centered noaction z100', document.body);
    let Message = CreateElement('div', 'message', '', Container);
    let Text = CreateElement('p', '', '', Message);
    let Options = CreateElement('div', '', 'row justifycenter', Message);

    let Buttons = [];
    for (let i = 0; i < 3; i++) {
        Buttons.push(CreateElement('button', '', '', Options));
    }

    document.body.appendChild(Container);

    this.Show = function (message, options, keepOn) {
        Text.innerHTML = message;

        if (options) {
            for (let i = 0; i < 3; i++) {
                if (options[i]) {
                    Buttons[i].innerHTML = options[i].text;
                    Buttons[i].onclick = () => { options[i].callback(); };
                    Buttons[i].style.display = 'block';
                }
                else Buttons[i].style.display = 'none';
            }
            Options.style.display = 'flex';
        }
        else Options.style.display = 'none';

        Message.style.display = 'flex';

        FadeCss(Message, 0.03, () => {
            if (!keepOn) setTimeout(() => { self.Close(); }, options ? 5000 : 1000);
        }, 0);
    }

    this.Close = function () {
        FadeCss(Message, -0.1, () => {
            Message.style.display = 'none';
        }, 1);
    }
}

let Message = new CreateMessage();