let FileSys = require('fs');
let Path = require('path');

function FSExt() { }

FSExt.FindDirsWithName = function (name, rootf, sub) {
    let found = [];
    let dirs = FSExt.GetAbs(rootf).filter(FSExt.IsDir);

    for (let i of dirs) {
        if (Path.basename(i) == name) {
            found.push(i);
            if (sub) found = found.concat(FSExt.GetAllDirs(i));
        }

        else found = found.concat(FSExt.FindDirsWithName(name, i, sub));
    }

    return found;
}

FSExt.GetAllDirs = function (rootf) {
    let found = [];
    let dirs = FSExt.GetAbs(rootf).filter(FSExt.IsDir);

    for (let i of dirs) {
        found.push(i);
        found = found.concat(FSExt.GetAllDirs(i));
    }

    return found;
}

FSExt.Find = function (filename, rootf) {
    let dirs = FSExt.GetAllDirs(rootf);
    for (let i of dirs)
        if (FileSys.existsSync(Path.join(i, filename)))
            return Path.join(i, filename);

    return "";
}

FSExt.GetAbsolute = function (rootf) {
    return FileSys.readdirSync(rootf)
        .map(path => Path.join(rootf, path));
}
FSExt.GetAbs = FSExt.GetAbsolute;

FSExt.Retrace = function (from, callback) {
    let lastfrom;

    while (lastfrom != from) {
        lastfrom = from;
        from = Path.dirname(from);
        if (callback(from)) break;
    }
}

FSExt.FindParentDir = function (path, name) {
    let out;
    FSExt.Retrace(path, (p) => {
        if (Path.basename(p) == name) {
            out = p;
            return true;
        };
    });
    return out;
}

FSExt.AllDirsCache = { rootf: "", dirs: "" };

FSExt.IsDir = (path) => FileSys.lstatSync(path).isDirectory();
FSExt.IsF = (path) => FileSys.lstatSync(path).isFile();
FSExt.IsImg = (path) => ['.png', '.jpg', '.psd'].includes(Path.extname(path));

module.exports = FSExt;