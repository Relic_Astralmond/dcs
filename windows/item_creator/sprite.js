function Sprite(data, sprite, fromsheet) {
    this.rel = fromsheet ? data.rel + "\\" + sprite.name : data.rel;

    this.data = data;
    this.sprite = sprite;

    this.SetForm = function (elem) {
        if (sprite) {
            elem.style.width = sprite.rect.width + "px";
            elem.style.height = sprite.rect.height + "px";

            let img = new Image();
            let h;
            img.name = data.url;
            img.onload = () => {
                h = img.height;
                elem.style.backgroundPosition = "-" + sprite.rect.x + "px -" + (h - sprite.rect.y - sprite.rect.height) + "px";
            };
            img.src = data.url;

            //elem.style.backgroundPosition = "-" + h - sprite.rect.x - sprite.rect.height + "px -" + sprite.rect.y + "px";
        }

        elem.style.backgroundImage = "url('" + data.formated + "')";
    }
}

Sprite.FromPath = function (path) {
    let spr = Ipc.sendSync('SPR:sprite', path);

    if (spr.sprite) return new Sprite(spr, spr.sprite);

    else if (spr.sheet) {
        let out = [];
        for (let sprite of spr.sheet.sprites)
            out.push(new Sprite(spr, sprite, true));
    }
}

Sprite.FromList = function (list) {
    let sprites = [];

    for (let i of list) {
        if (i.sheet)
            for (let spr of i.sheet.sprites)
                sprites.push(new Sprite(i, spr, true));
        else sprites.push(new Sprite(i));
    }

    return sprites;
}