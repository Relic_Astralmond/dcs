function Item(loaded, setform) {
    let self = this;

    this.type = Item.Types.GetByName(loaded.type || 'null');
    this.id = loaded.id || 0;
    this.name = loaded.name || "Unnamed";
    this.desc = loaded.desc || "An Item";
    this.value = loaded.value || 0;
    this.sprite = Sprite.FromPath(loaded.sprite || "");
    this.prefab = new Prefab(loaded.prefab || "");
    this.stats = new Stats(loaded.stats || []);
    this.durability = loaded.durability || 0;

    this.dom = document.createElement('li');
    this.dom.innerHTML = this.name;
    this.dom.onclick = () => { self.SetForm(); };

    this.SetForm = function () {
        Item.Selected = self;

        inp_type.selectedIndex = self.type.id;
        inp_name.value = self.name;
        inp_id.value = self.id;
        inp_desc.value = self.desc;
        if (self.sprite)
            self.sprite.SetForm(inp_sprite);
        self.prefab.SetForm(inp_sprite);
        inp_val.value = self.value;
        inp_durability.value = self.durability;
        self.stats.SetForm();
    }

    this.Remove = function () {
        self.dom.parentNode.removeChild(self.dom);
        Item.list.splice(Item.list.indexOf(self));
        if (Item.list[0]) Item.list[0].SetForm();
    }

    items.appendChild(this.dom);

    Item.list.push(this);

    if (setform) this.SetForm();
}

Item.Selected;

Item.Types = new NamesList();
Item.Types.Add(['Special', 'special', 'spc'], 0, 2, 0, 1);
Item.Types.Add(['Potion', 'potion', 'pot'], 1, 2, 0, 1);
Item.Types.Add(['Weapon', 'weapon', 'wpn'], 2, 2, 0, 1);
Item.Types.Add(['Shield', 'shield', 'shld'], 3, 2, 0, 1);
Item.Types.Add(['Chest', 'chest', 'ch'], 4, 2, 0, 1);
Item.Types.Add(["Undefined", "undefined", "null"], -1, 2, 0, 1);
Item.Types.SetSelect(inp_type);

Item.Clean = function () {
    if (!Item.Check()) return;
    let list = [];

    for (let item of Item.list) {
        console.log(item.name);
        list.push({
            type: item.type.common,
            id: item.id,
            name: item.name,
            desc: item.desc,
            value: item.value,
            sprite: item.sprite.rel,
            prefab: item.prefab.path,
            stats: Stats.Clean(item.stats),
            durability: item.durability
        });
    }
    console.log(list);

    return list;
}

Item.Check = function () {
    let Error = function (message, item) {
        Message.Show(message);
        item.SetForm();
        return false;
    }

    let ids = new Map();
    for (let i of Item.list) {
        if (!i.name) { return Error("An item has no name", i); }
        if (!i.id && i.id !== 0) { return Error("An item has no id", i); }
        if (ids.has(Number(i.id))) { return Error(ids.get(Number(i.id).name + " and " + i.name + 'have the same ID (' + i.id + ')', i)); }

        for (let stat of i.stats) {
            if (stat.target.id == -1) { return Error("An undefined stat exists", i); }
        }

        ids.set(Number(i.id), i);
    }

    console.log("CHECK PASS");
    return true;
}

Item.list = [];