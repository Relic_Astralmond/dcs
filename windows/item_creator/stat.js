function Stat(loaded, setform) {
    let self = this;

    this.target = Stat.Types.GetByName(loaded.target || 'null');
    this.dur = loaded.dur || 0;
    this.raw = loaded.raw || 0;
    this.mult = loaded.mult || 0;
    this.replenish = loaded.replenish || 0;

    this.dom = document.createElement('li');
    this.dom.innerHTML = this.target.descriptive;
    this.dom.onclick = () => { self.SetForm(); };

    this.SetForm = function () {
        Stat.Selected = self;

        inp_stat.selectedIndex = self.target.id;
        inp_raw.value = self.raw;
        inp_mult.value = self.mult;
        inp_rep.value = self.replenish;
        inp_dur.value = self.dur;
    }

    this.Remove = function () {
        Item.Selected.stats.splice(Item.Selected.stats.indexOf(self));
        if (Item.Selected.stats[0]) Item.Selected.stats[0].SetForm();
    }

    if (setform) this.SetForm();
}

Stat.Add = function () {
    Item.Selected.stats.push(new Stat({}, true));
    Item.Selected.stats.SetForm();
}


Stat.Selected;

Stat.Types = new NamesList();
Stat.Types.Add(["Health", "health", "hp"], 0, 2, 0, 1);
Stat.Types.Add(["Health Regen", "health_regen", "hpregen", "hpr", "hpreg"], 1, 3, 0, 1);
Stat.Types.Add(["Mana", "mana", "mp"], 2, 2, 0, 1);
Stat.Types.Add(["Mana Regen", "mana_regen", "mpregen", "mpr", "mpreg"], 3, 3, 0, 1);
Stat.Types.Add(["Stamina", "stamina", "sta"], 4, 2, 0, 1);
Stat.Types.Add(["Stamina Regen", "stamina_regen", "staregen", "star", "stareg"], 5, 2, 0, 1);
Stat.Types.Add(["Attack", "attack", "att", "atk"], 6, 2, 0, 1);
Stat.Types.Add(["Defence", "defence", "defense", "def"], 7, 3, 0, 1);
Stat.Types.Add(["Armor", "armor", "armour", "arm"], 8, 3, 0, 1);
Stat.Types.Add(["Speed", "speed", "spd"], 9, 2, 0, 1);
Stat.Types.Add(["Undefined", "undefined", "null"], -1, 2, 0, 1);
Stat.Types.SetSelect(inp_stat);

function Stats(loaded) {
    let self = [];

    for (let i of loaded)
        self.push(new Stat(i));

    self.SetForm = function () {
        ClearChildren(stats);

        for (let i of self) {
            if (!i.dom) continue;
            i.dom.onclick();
            stats.appendChild(i.dom);
        }
    }

    return self;
}

Stats.Clean = function (stats) {
    let out = [];

    for (let i of stats) {
        if (!i.dom) continue;

        out.push({
            raw: i.raw,
            mult: i.mult,
            replenish: i.replenish,
            dur: i.dur,
            target: i.target.common
        });
    }

    return out;
}