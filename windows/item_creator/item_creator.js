let Window = require('./../window');
let Selector = require('./../sprite_selector/selector');
let SpriteDatabase = require('./../sprite_selector/database');

let Electron = require('electron');
let Ipc = Electron.ipcMain;
let Dialog = Electron.dialog;

let FileSys = require('fs');
let FSExt = require('./../../modules/fsext');

let Instance;

function ItemCreator() {
    let self = new Window('item_creator/index.html');

    self.WorkDir;
    self.ItemFile;
    self.SpriteDB = new SpriteDatabase(self);

    self.OpenProject = function () {
        Dialog.showOpenDialog(self, { properties: ['openDirectory', 'createDirectory'] }, (dirs) => {
            if (!dirs) return;

            self.WorkDir = dirs[0];
            self.SpriteDB.Load(self.WorkDir);

            ItemFile = FSExt.Find("items.dcs", self.WorkDir);
            if (ItemFile) { self.OpenItem(); }
            else self.webContents.send('IC:noitem');
        });
    }

    self.OpenItem = function () {
        let data = FileSys.readFileSync(ItemFile);
        let json = data.length > 0 ? JSON.parse(data) : {};
        self.webContents.send('IC:load', json);
    }

    self.LoadItem = function () {
        Dialog.showOpenDialog(self, { properties: ['openFile'] }, (filePaths) => {
            if (!filePaths) return;
            ItemFile = filePaths[0];
            self.OpenItem();
        });
    }

    self.ExportItem = function (items) {
        console.log("AAA");
        Dialog.showSaveDialog(self, {}, (path) => {
            if (!path) return;
            FileSys.writeFile(path, JSON.stringify(items, null, '\t'), () => {
                self.webContents.send('IC:saved');
            });
        });
    }

    self.NewItem = function () { ; };

    self.GetSprite = function () {
        let s = new Selector(Instance, self.SpriteDB);
    }

    Ipc.on('SPR:sprite', (event, arg) => {
        event.returnValue = self.SpriteDB.Get(arg);
    });

    Ipc.on('IC:export', (event, args) => {
        self.ExportItem(args);
    });

    self.Save = function () {
    }

    Instance = self;
    return self;
}

module.exports = ItemCreator;