function NamesList() {
    let Names = this.Names = new Map();
    let Ids = this.Ids = new Map();

    this.Add = function (names, id, short, desc, common) {
        let entry = new Entry(names, id, short || 2, desc || 0, common || 1);

        Ids.set(id, entry);
        for (let i of names) Names.set(i, entry);
    }

    this.GetById = function (id) {
        return Ids.get(Number(id));
    }

    this.GetByName = function (name) {
        return Names.get(name);
    }

    this.SetSelect = function (select) {
        for (let entry of Ids.values())
            if (entry.id != -1)
                select.innerHTML += '<option value=' + entry.id + '>' + entry.descriptive + '</option>';
    }

    let Entry = function (names, id, short, desc, common) {
        this.names = names;
        this.short = names[short];
        this.descriptive = names[desc];
        this.common = names[common];
        this.id = id;
    }
}