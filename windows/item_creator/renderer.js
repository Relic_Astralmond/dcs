let Electron = require('electron');
let Ipc = Electron.ipcRenderer;
let Remote = Electron.remote;

let win = Remote.getCurrentWindow();
Sprite.Win = win;

let Verts = Vertices(verts_canvas, 0.00007, 170, {
    dot: new Color(64, 64, 64, 1),
    line: new Color(64, 64, 64, 1)
});

function Export() {
    Ipc.send('IC:export', Item.Clean());

    //win.ExportItem(Item.Clean());
}

inp_type.onchange = () => {
    Item.Selected.type = Item.Types.GetById(inp_type.options[inp_type.selectedIndex].value);
}

inp_name.onchange = () => {
    Item.Selected.name = inp_name.value;
    Item.Selected.dom.innerHTML = Item.Selected.name;
}

inp_id.onchange = () => { Item.Selected.id = inp_id.value; }
inp_desc.onchange = () => { Item.Selected.desc = inp_desc.value; }
inp_val.onchange = () => { Item.Selected.value = inp_val.value; }
inp_durability.onchange = () => { Item.Selected.durability = inp_durability.value };


inp_stat.onchange = () => {
    Stat.Selected.target = Stat.Types.GetById(inp_stat.options[inp_stat.selectedIndex].value);
    Stat.Selected.dom.innerHTML = Stat.Selected.target.descriptive;
}


inp_raw.onchange = () => { Stat.Selected.raw = inp_raw.value; }
inp_mult.onchange = () => { Stat.Selected.mult = inp_mult.value; }
inp_rep.onchange = () => { Stat.Selected.replenish = inp_rep.value; }
inp_dur.onchange = () => { Stat.Selected.dur = inp_dur.value; }


Ipc.on('IC:saved', () => { Message.Show("Succesfully saved"); });

Ipc.on('SS:chosen', (event, args) => {
    Item.Selected.sprite = Sprite.FromPath(args);
    Item.Selected.sprite.SetForm(inp_sprite);
});

Ipc.on('IC:noitem', () => {
    let options = [
        {
            text: "Find item file",
            callback: () => { win.LoadItem(); Message.Close(); }
        }, {
            text: "Create new",
            callback: () => { Message.Show("Not implemented, sorry."); }
        }
    ];
    Message.Show('No Item File Found', options, true);
});

Ipc.on('IC:load', (event, arg) => {
    for (let i of Object.values(arg)) new Item(i);
    if (Item.list[0]) Item.list[0].SetForm();

    item.style.display = 'flex';
    FadeCss(item, 0.03, () => { }, 0);
    FadeCss(demi_logo, -0.02, () => {
        demi_logo.style.display = 'none';
    }, 1);
});
