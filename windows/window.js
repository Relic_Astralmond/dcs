let Electron = require('electron');
let Window = Electron.BrowserWindow;
let Ipc = Electron.ipcMain;

let Url = require('url');
let Path = require('path');

function DWindow(page) {
    let self = new Window({ width: 800, height: 600, frame: false });

    self.Load = function () {
        self.loadURL(Url.format({
            pathname: Path.join(__dirname, page),
            protocol: "file:",
            slashes: true
        }));
    }

    self.Load();
    self.on('closed', () => { win = null; });

    self.devTools = function () {
        let bounds = self.getBounds();
        bounds.width += 300;
        self.setBounds(bounds);

        self.webContents.openDevTools();
    }

    return self;
}



module.exports = DWindow;