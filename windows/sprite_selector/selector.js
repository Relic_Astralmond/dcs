let Window = require('./../window');
let Database = require('./database');

let Electron = require('electron');
let Ipc = Electron.ipcMain;

let Instance;

function Selector(caller, database) {
    let self = new Window('sprite_selector/index.html');

    self.Choose = (path) => {
        caller.webContents.send('SS:chosen', path);
        self.close();
    }

    self.webContents.on('did-stop-loading', () => { self.webContents.send('SS:select', Array.from(database.loaded.values())); });
    Instance = self;
    return self;
}

module.exports = Selector;