let FileSys = require('fs');
let FSExt = require('./../../modules/fsext');
let Path = require('path');
let Url = require('url');

let YAML = require('yamljs');

let Instance;
let Window;

function SpriteDatabase(win) {
    let self = this;
    this.loaded = new Map();
    this.path = "";

    this.Load = function (path) {
        self.path = path;

        let dirs = FSExt.FindDirsWithName('Resources', path, true);

        for (let dir of dirs) {
            let files = FSExt.GetAbsolute(dir).filter(x => FSExt.IsF(x) && FSExt.IsImg(x));

            for (let file of files)
                self.loaded.set(file, new Sprite(file));
        }
    }

    this.Reload = function () {
        self.loaded = [];
        self.Load(self.path);
    }

    this.Get = function (file) {
        console.log("F: " + file);

        if (!file) return {};

        if (self.loaded.has(file)) return self.loaded.get(file);
        else return new Sprite(file);
    }

    Instance = this;
    Window = win;
}


let Sprite = function (file) {
    let out = {};

    let sub;
    if (Path.extname(file) == '') {
        sub = Path.basename(file);
        file = Path.dirname(file);
    }

    if (!Path.isAbsolute(file)) {
        let allresources = FSExt.FindDirsWithName('Resources', Window.WorkDir);
        let possibilities = allresources.map(f => Path.join(f, file));
        possibilities.filter(f => FileSys.exists(f));

        if (possibilities[0]) file = possibilities[0];
        else console.log("SPRITE FILE NOT FOUND: " + file);
    }


    if (!FileSys.existsSync(file + '.meta')) return;

    out.path = file;
    out.rel = Path.relative(FSExt.FindParentDir(file, 'Resources'), file);
    out.url = Url.format({ pathname: file, protocol: 'file:', slashes: true });
    out.formated = out.url.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");

    let meta = YAML.load(file + '.meta').TextureImporter;

    if (meta.spriteMode == 2) {
        if (sub) {
            out.sprite = meta.spriteSheet.sprites.filter(spr => spr.name == sub)[0];
            out.rel += "\\" + out.sprite.name;
        }
        else out.sheet = meta.spriteSheet;
    }

    return out;
}

module.exports = SpriteDatabase;