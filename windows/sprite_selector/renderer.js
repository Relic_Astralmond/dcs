let Electron = require('electron');
let Ipc = Electron.ipcRenderer;
let Remote = Electron.remote;

let win = Remote.getCurrentWindow();

let Send = (msg) => { Ipc.send(msg); }

let Verts = Vertices(verts_canvas, 0.00007, 170, {
    dot: new Color(64, 64, 64, 1),
    line: new Color(64, 64, 64, 1)
});

function MakeSprite(sprite) {
    let cont = document.createElement('div');
    cont.onmouseenter = () => { p_path.innerHTML = sprite.rel; }
    cont.onclick = () => { win.Choose(sprite.rel); }

    let img = document.createElement('div');
    sprite.SetForm(img);
    cont.appendChild(img);
    grid.appendChild(cont);
}

Ipc.on('SS:select', (event, args) => {
    let list = Sprite.FromList(args);
    for (let i of list) MakeSprite(i);
});