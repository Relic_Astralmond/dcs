let Electron = require('electron');
let App = Electron.app;

let SpriteDatabase = require('./windows/sprite_selector/database');
let ItemCreator = require('./windows/item_creator/item_creator');
let SpriteSelector = require('./windows/sprite_selector/selector');

let ic;

App.on('ready', () => { ic = new ItemCreator(); });
App.on('window-all-closed', App.quit);
App.on('activate', () => { if (ic === null) ic = new ItemCreator(); });

